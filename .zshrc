# {{{ SSH multiplexing
if (( $+SSH_CONNECTION & ~$+TMUX_VERSION & ~$+SCREEN & ~$+DTACH )); then
	if (( $+commands[tmux] )); then
		target="$(tmux list-sessions -F "#S" 2>/dev/null)"
		if ! (( $? )) ; then
			session="$(tmux new-session -t "${target%%$'\n'*}")"
			[[ "$session" = \[detached* ]] && tmux kill-session -t "${session:24: -2}"
		else tmux new-session; fi
	elif (( $+commands[dtach] )); then
		export DTACH="$PREFIX/tmp/$USER-dtach"
		dtach -A "$DTACH" zsh
		unset DTACH
	fi
fi
# }}}
# {{{ Termux-specific
if (( ${+ANDROID_DATA} )); then
	:
else
	:
fi
# }}}
# {{{ P10k instant prompt
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# }}}
. $ZDOTDIR/.zprofile

autoload -Uz add-zsh-hook
# {{{ Terminal title on cd
function .set-title(){
	if [[ -v TMUX ]]; then
		print -P '\e]0;%~\a'
	else
		print -P '\e]0;'"${TERM:+${TERM[1]:u}${TERM[2,-1]:l} - }${SSH_CONNECTION+%n@%M}"'%~\a'
	fi
}
.set-title
add-zsh-hook chpwd .set-title
# }}}
# {{{ Interactive vars/options
HISTSIZE=50000
HISTFILE="$ZDOTDIR/zsh_history"
KEYTIMEOUT=20 # 200 ms
SAVEHIST=10000
eval "$(TERM=xterm dircolors -b)"

export BROWSER=xdg-open
export FZF_DEFAULT_OPTS="--bind=\"ctrl-j:preview-down,ctrl-k:preview-up,ctrl-alt-j:preview-page-down,ctrl-alt-k:preview-page-up,ctrl-a:select-all,ctrl-alt-a:deselect-all,alt-a:toggle-all,alt-e:execute($EDITOR {+})\""
export HISTORY_IGNORE="(#i)(cd|cd ..|clear|g ca#m *|l[sal]#( *)#|ldot|exit)"
export RIPGREP_CONFIG_PATH="$HOME/.config/rg.conf"
ZCALCPROMPT="%F{5}%1v%F{reset}:%F{11}λ%F{reset} "
ZSH_AUTOSUGGEST_USE_ASYNC="true"
ZSH_EVIL_SYNC_EDITOR="$EDITOR"

setopt autocd autopushd noclobber
setopt extended_history       # record timestamp of command in HISTFILE
setopt interactivecomments rcquotes
setopt extendedglob globstarshort
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt promptsubst
setopt share_history          # share command history data
setopt cbases octalzeroes     # 0x[hex], 0[octal]
# }}}
# {{{ Zinit
for dir in $gitrepopath; do
	if [[ -d $dir/zinit ]]; then
		source $dir/zinit/zinit.zsh
		module_path+=( "$dir/zinit/zmodules/Src" )
		zmodload zdharma/zplugin
		break
	fi
done

zinit ice compile"*.lzui"
zinit load zdharma/zui

zinit light xPMo/zsh-prompt-dir-glob

zinit ice wait"1"
zinit light xPMo/zsh-toggle-command-prefix

zinit ice depth=1
zinit light romkatv/powerlevel10k
source $ZDOTDIR/.p10k.zsh

zinit ice wait"1"
zinit light MichaelAquilina/zsh-you-should-use

zinit ice wait "0" ver"dev"
zinit light zsh-vi-more/evil-registers

zinit ice wait "0" ver"dev"
zinit light zsh-vi-more/vi-motions
## (forward|backward)-blank-word bindings:
for m in vicmd visual; do
	bindkey -M "$m" 'W'  vi-forward-shell-word
	bindkey -M "$m" 'B'  vi-backward-shell-word
	bindkey -M "$m" 'E'  vi-forward-shell-word-end
	bindkey -M "$m" 'gE' vi-backward-shell-word-end
done

zinit ice wait "0" ver"dev"
zinit light zsh-vi-more/vi-increment

zinit ice wait "0" ver"dev"
zinit light zsh-vi-more/vi-quote

zinit ice wait "0"
zinit light $ZDOTDIR/plugins/vi-mode

zinit ice wait "1"
zinit light $ZDOTDIR/plugins/aliases

zinit ice wait "0"
zinit load $ZDOTDIR/plugins/colored-man-pages

zinit ice wait"0"
zinit light $ZDOTDIR/plugins/i3tool

zinit ice wait"1"
zinit load $ZDOTDIR/plugins/my

zinit ice wait"1" blockf
zinit light thetic/extract

zinit ice wait"0"
zinit snippet ${PREFIX:-/usr}/share/fzf/completion.zsh

zinit ice wait"0"
zinit snippet ${PREFIX:-/usr}/share/fzf/key-bindings.zsh

zinit ice wait"0"
zinit light zdharma/fast-syntax-highlighting

zinit ice wait"0"
zinit light zsh-users/zsh-autosuggestions
# }}}
# {{{ Completion
autoload -U zmv zcalc zargs
autoload -Uz compinit
compinit

# completion style
zmodload -i zsh/complist
bindkey -M menuselect '^o' accept-and-infer-next-history
zstyle ':completion:*:*:*:*:*' menu select
(( ${+LS_COLORS} )) || eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# Autocomplete on hidden(dot) files
_comp_options+=(globdots)

# }}}
source $ZDOTDIR/zdirs

[[ -n "$startup" ]] && eval ${startup} || true
# terminal init:
.on_winch(){
	local s
	# tabs
	s+='\e[g'
	repeat $(( COLUMNS / $1 )) s+='\e['$1'D\eH'

	# final
	print -n '\e7'$s'\e8'
}
.on_winch 4
trap '.on_winch 4' WINCH
#trap 'echo 4' WINCH
# vim: set foldmethod=marker:
