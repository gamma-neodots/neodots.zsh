fpath+="${0:h}"
ZVSC_EDITOR=nvr

# {{{ Standard bindkey fixes
#https://www.reddit.com/r/zsh/comments/eblqvq/del_pgup_and_pgdown_input_in_terminal/fb7337q/

# If NumLock is off, translate keys to make them appear the same as with NumLock on.
bindkey -s '^[OM' '^M'  # enter
bindkey -s '^[Ok' '+'
bindkey -s '^[Om' '-'
bindkey -s '^[Oj' '*'
bindkey -s '^[Oo' '/'
bindkey -s '^[OX' '='

# If someone switches our terminal to application mode (smkx), translate keys to make
# them appear the same as in raw mode (rmkx).
bindkey -s '^[OH' '^[[H'  # home
bindkey -s '^[OF' '^[[F'  # end
bindkey -s '^[OA' '^[[A'  # up
bindkey -s '^[OB' '^[[B'  # down
bindkey -s '^[OD' '^[[D'  # left
bindkey -s '^[OC' '^[[C'  # right

# TTY sends different key codes. Translate them to regular.
bindkey -s '^[[1~' '^[[H'  # home
bindkey -s '^[[4~' '^[[F'  # end
# }}}

# Updates editor information when the keymap changes.
function zle-keymap-select() {
	case $KEYMAP$ZLE_STATE in
		# block
		vicmd*) print -n "\e[2 q" ;;
		# bar
		*insert*) print -n "\e[6 q" ;;
		# underbar
		*) print -n "\e[4 q" ;;
	esac
}

# sets keymap on next commandline
zle-line-init() {
	zle -K viins
}
zle -N zle-line-init

# Ensure that the prompt is redrawn when the terminal size changes.

zle -N zle-keymap-select
zle zle-keymap-select

bindkey -v

# allow :e to edit the command line
autoload -Uz edit-command-line
if [[ $VISUAL = nvr* ]]; then
	edit-command-line-nvr(){ VISUAL="$VISUAL --remote-wait" edit-command-line }
	zle -N edit-command-line{,-nvr}
else
	zle -N edit-command-line
fi
local a
for a in e ed edi edit; do
	zle -A edit-command-line $a
done

# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
bindkey '^P' up-history
bindkey '^N' down-history

# allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
bindkey '^?'    backward-delete-char
bindkey '^h'    backward-delete-char
bindkey '^[[3~' delete-char
bindkey '^w'    backward-kill-word

# allow ctrl-r to perform backward search in history
bindkey '^r' history-incremental-search-backward

# allow ctrl-a and ctrl-e to move to beginning/end of line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line

# pipe a text selected by a motion through another command
autoload -Uz vi-pipe
zle -N vi-pipe
bindkey -a '!' vi-pipe

#vim: set foldmethod=marker:
