[[ -e "$HOME/.local/lib/i3/i3tool.sh" ]] && {
	. "$HOME/.local/lib/i3/i3tool.sh"
	alias i3=i3tool
	alias i3m='i3tool msg'
	i3e(){
		i3tool --no-startup-id -- exec "${1:c:P}" "${@:2}"
	}
}
