# {{{ Quick access to zshrc, aliases, and history
alias ezrc="$VISUAL ${ZDOTDIR:-$HOME}/.zshrc"
alias ezshrc="$VISUAL ${ZDOTDIR:-$HOME}/.zshrc"
function eal(){
	(( ${+1} )) &&
		$VISUAL ${(%):-%x} -c "/$1=" ||
		$VISUAL ${(%):-%x}
}
compctl -a eal

alias ehist="$VISUAL $HISTFILE"
alias h='history'
alias hgrep="fc -El 0 | grep"
# }}}
# {{{ ls aliases
alias ls='ls --color=auto'
alias l='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable
alias lA='ls -laFh'   #long list,show all,show type,human readable
alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable
alias lls='ls -lSFh'  #long list,sorted by size,show type,human readable
alias ll='ls -l'      #long list
alias ldot='ls -ld .*'
alias lS='ls -1FSsh'
alias lart='ls -1Fcart'
alias lrt='ls -1Fcrt'

alias lf='ls -AF'    # list almost all, include type
alias lat='ls -ltuh' # sort by access time
alias lct='ls -ltch' # sort by ctime
alias sl='ls -r'     # no steam locomotive here
alias lsd='ls -C | column --table | lolcat'
function § { ls --color -C * $@ | less -R } # depth=1
# }}}
# {{{ sudo
alias _='sudo'
alias _i='sudo -i'
alias _u='sudo su'
alias _e='sudoedit'
compdef gksudo='sudo'
# }}}
# {{{ Grep aliases
if (( $+commands[rg] )); then
	alias grep='rg -N'
	alias sgrep='rg -n -C 5'
elif (( $+commands[ag] )); then
	alias grep='ag --nonumbers'
	alias sgrep='ag -C 5'
else
	alias grep='grep --color'
	# use grep as a cheap rg/ag replacemnet
	alias ag='grep -R -n -H --exclude-dir={.git,.svn,CVS}'
	alias rg='grep -R -n -H --exclude-dir={.git,.svn,CVS}'
	alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS}'
fi
# }}}
# {{{ Globals
alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L="| less"
alias -g LL="2>&1 | less"
alias -g CA="2>&1 | cat -A"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"
# alias -g P="2>&1| pygmentize -l pytb"
# }}}
# {{{ fd / find
alias find='noglob find'
if (( $+commands[fd] )); then
	alias fd='noglob fd --glob --type directory'
	alias ff='noglob fd --glob --type file'
	alias fl='noglob fd --glob --type symlink'
else
	alias fd='noglob find . -type d -name'
	alias ff='noglob find . -type f -name'
	alias fl='noglob find . -type l -name'
fi
# }}}
# alias help='man'
alias p='ps -f'
alias about='id; hostname --long; cat /etc/*-release'
alias whereami=display_info

# {{{ File operations
zmodload zsh/files # zsh versions of mv, rm, mkdir, etc
alias ppwd='print -P %~'
alias rm='rm -s'   # zsh extension, enable paranoid behavior
alias trash='gio trash'
alias cp='cp -i --reflink=auto' # lightweight copy when possible
alias mv='mv -i'
alias rscp='rsync -aP --human-readable --info=flist0,progress2,stats1'
function RSCP {
	rsync -aP "${1:-desktop.zt}:$(print -P %~)/${2:-.}/*" ${2:-.}
}

# regex rename with zmv
alias zmv='noglob zmv'
alias mmv='noglob zmv -W'

# }}}
alias crontab='crontab -i'
# {{{ ZSH suffix magic
if is-at-least 4.2.0; then
	# open browser on urls
	[[ "$BROWSER"  ]] && alias -s {htm,html,de,org,net,com,at,cx,nl,se,dk}=$BROWSER
	[[ "${VISUAL:-$EDITOR}" ]] && alias -s {cpp,cxx,cc,c,hh,h,inl,asc,md,txt,TXT,tex}=${VISUAL:-$EDITOR}
	[[ "$XIVIEWER" ]] && alias -s {jpg,jpeg,png,gif,mng,tiff,tif,xpm}=$XIVIEWER

	[[ $commands[mpv] ]] && alias -s {ape,avi,flv,m4a,mkv,mov,mp3,mp4,mpeg,mpg,ogg,ogm,rm,wav,webm}=mpv

	#read documents
	alias -s pdf=zathura ps=gv dvi=xdvi chm=xchm djvu=djview

	#list whats inside packed file
	alias -s zip="unzip -l" rar="unrar l" tar="tar tf" tar.gz="echo " ace="unace l"
fi
# }}}
# {{{ ssh hosts
zstyle -e ':completion:*:(mosh|ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'
# }}}
# {{{ Android / TTS
if (( ${+ANDROID_DATA} )); then
	alias tts=termux-tts-speak
	alias ssh=ssha
elif (( $+commands[pico2wave] )); then
	function tts() {
		local f=$(mktemp --suffix=.wav)
		trap '{ rm "$f"; exit }' INT
		pico2wave -w "$f" -- "$*"
		paplay "$f"
		rm "$f"
	}
	alias tts='noglob tts'
elif (( $+commands[mimic] )); then
	function tts() {
		mimic "$*" play
	}
	alias tts='noglob tts'
fi
# }}}
# {{{ Terminal utils
# font size for urxvt
alias sfs='urxvt-set -f'
# force clear all terminals
alias clearall='clear | tee /dev/{tty*,pts/*}'
alias -- +x='chmod u+x,g+x'
alias -- -x='chmod -x'
# }}}
# {{{ git: use aliases in gitconfig
alias g='git'
alias gs='git s' # tired of ghostscript on accident
alias gg='cd $(git root)'
# }}}
# {{{ fzf
# Use tmux version if in tmux session
if (( ${+TMUX} )) && FZF_TMUX=1; then
	alias fzf='fzf-tmux'
fi
# which completion
alias whichzf='which -S $(print -l ${(k)commands} ${(k)aliases} ${(k)functions} | sort -u | fzf -m)'
alias wh=which
# use locate database (`cut` off the working dir)
if (( $+commands[locate] )); then
	export FZF_DEFAULT_COMMAND='p=$(realpath $(pwd)); command locate "$p" | cut -b$(( ${#p} + 2 ))-'
	alias fzfa='locate "*" | fzf'
fi
# fzf-grep: TODO: replace with zsh widget
function 'ffg' {
	rg --files-with-matches "$@" | \
		fzf --multi --preview-window=up:60% \
		--preview "rg --color=ansi --line-number --context 1 $(
			printf "'%s' " "$@"
		){+}" \
		--bind="alt-e:execute($VISUAL {+} -c \":silent grep! $(
			printf "'%s' " "$@"
		)\" -c \":copen\")"
} # group as one string
# }}}
# {{{ user-mount using udisksctl
alias ud=udisksctl
function udm {
	cd "$(udisksctl mount -b "$@" | tee /dev/stderr | sed 's/.* at \(.*\).$/\1/')"

}
function udum { # unmount by /dev/ or by mount path
	udisksctl unmount -b "$@" || udisksctl unmount -p "$@"
}
compdef _mount udm=mount
compdef _mount udum=umount
# }}}
# {{{ managing executables
d() {
	# cover no arg case
	if ! (( $# )) || [[ -d "$1" ]]; then
		# d: cd to path
		cd "$@"
	elif [[ -e "$1" ]]; then
		# *: cd to parent
		cd "${1:h}"
	elif mkdir -p "$1"; then
		# N: new directory
		echo "created directory ${1:q}"
		cd "$1"
	fi
}
D() { # d, but absolute
	if ! (( $# )) || [[ -d "$1" ]] then
		cd -P "$@"
	elif [[ -e "$1" ]] then
		cd "${1:P:h}"
	elif mkdir -p "$1"; then
		echo "created directory ${1:q}"
		cd -P "$1"
	fi
}
compdef _files d
compdef _files D
function readlink {
	local a
	echo -n "$1"
	a="$(command -p readlink "$1")"
	[[ -z "$a" ]] && { echo; return; }
	echo -n ' -> '
	readlink "$a"
}
compdef _files readlinks
# if an executable expects to be launched in its directory
function cdex {
	local name
	[[ -f "$1" ]] && name="${1:P}" || name="$(which "$1")"
	cd "${name:P:h}"; "$name" "${@:2}"
}
# }}}
# {{{ grc colouring
alias GRC='grc -es'
alias LA='GRC ls -lFAhb --color'
alias LS='GRC ls -lFhb --color'
alias df='GRC df -hT'
alias dig='GRC dig'
alias docker='GRC docker'
alias docker-machine='GRC docker-machine'
alias env='GRC env'
alias gcc='GRC gcc'
alias ip='GRC ip'
alias lsblk='GRC lsblk'
alias lspci='GRC lspci'
alias mount='GRC mount'
alias lsmount='\mount | rg --color=never "^/" | grcat "${PREFIX:-/usr}/share/grc/conf.mount"'
alias nmap='GRC nmap'
alias ping='GRC ping'
alias ps='GRC ps --columns $COLUMNS'
alias traceroute='GRC traceroute'
du (){
	tabs -5
	local s='-s'
	local -a args
	trap '{tabs -4;}' INT
	while (( $# )); do
		case $1 in
			--) break ;;
			-d*|--max-depth*) unset s ;&
			*) args+=("$1") ;;
		esac
		shift
	done
	if (( $#args ))
	then args=(-h $s "${args[@]}" )
	else args=(-h -d 1 .)
	fi
	# if no args, depth=1 on cwd
	acol du "${args[@]}" "$@" | sort -h -k1.10
	tabs -4
}
function lsl { grc ls -lFhb "$@" --color | less -R }
compdef _ls lsl='ls -lFhb'
# }}}
# {{{ other colouring
function grepl { grep "$@" --color=always | less -R }
compdef _grep grepl='grep --color=always'
function hi { highlight -O ansi "$@" 2>/dev/null | less -R -F }
# }}}
# {{{ UU: update everything
# tmux split git updates from package-manager udpates
if (( ${+TMUX} )); then
	if (( $+commands[pikaur] )); then
		alias UU='tmux split git-all -w pu; pikaur -Syu'
	elif (( $+commands[aurman] )); then
		alias UU='tmux split git-all -w pu; aurman -Syu'
	elif (( $+commands[pkg] )); then
		alias UU='tmux split git-all -w pu; pkg update'
	elif (( $+commands[apt] )); then
		alias UU='tmux split git-all -w pu; sh -c "sudo apt update && sudo apt full-upgrade"'
	fi
	alias peek='tmux split-window -p 30 $EDITOR'
else
	if (( $+commands[pikaur] )); then
		alias UU='tmux new git-all -w pu \; split pikaur -Syu'
	elif (( $+commands[aurman] )); then
		alias UU='tmux new git-all -w pu \; split aurman -Syu'
	elif (( $+commands[pkg] )); then
		alias UU='tmux new git-all -w pu \; split pkg update'
	elif (( $+commands[apt] )); then
		alias UU='tmux new git-all -w pu \; split sh -c "sudo apt update && sudo apt upgrade"'
	fi
fi
# }}}
# {{{ wrappers/defaults
# Shortened pythons
alias py3='python3'
alias py2='python2'

alias lkb='xmodmap -pke; xmodmap'           # List current keybindings
alias htip='HTOPRC=$HOME/.config/htop/minrc htop'
alias mpvr='mpv **(.) --audio-display=no'
function ttv {
	i3-msg exec "qutebrowser --target tab 'https://twitch.tv/popout/$1/chat?popout='"
	streamlink "twitch.tv/$1" "${@:2}"
}
alias ytdl='noglob youtube-dl'
alias nterm='nohup urxvt >/dev/null & exit' # launch new terminal in .
alias pycalc='python3 -ic "from cmath import *"'
alias rust-c='rustc --out-dir build -O'
alias zcalc='noglob zcalc'
function gc-c { gcc -O3 "$1" -o "build/${1%.*}" "${@:2}" }
function gh-c { ghc -Odph -dynamic "$1" -o "build/${1%.*}" -odir .build -hidir .build -tmpdir . "${@:2}" }
function hscalc { echo -e "main=putStrLn $ show $ $@" | runhaskell }
function join_by { local IFS="$1"; shift && echo "$*" }
function notefo { notes find $1 | notes open }
function redditcodecopy {
	sed -e 's/^/    /' -e 's/\t/    /g' "$@" |
	if (( $+WAYLAND_DISPLAY )); then wl-copy
	elif (( $+DISPLAY )); then xsel -b
	else cat; fi
}
function xevkb { xev -event keyboard "$@" | egrep -o '(keycode(.)+\)|XLookup.+[1-9].+)' }
compdef _x_utils xevkb='xev -event keyboard'
splitarray() {
   # Usage: split "array" "string" "delimiter"
   local IFS=$'\x1c' # 0x1c is file seperator
   read -d "" -rA "$1" <<< "${2//$3/$IFS}"
}
# }}}
# {{{ pointless
alias hs='while read line; do sh -c "$(echo $line | rev)"; done < "${1:-/dev/stdin}"'
alias SIDEWHEELIE='mpv ~/.local/share/audio/AnnouncerSideWheelie.ogg --msg-level=all=fatal --term-osd=no & disown'
# }}}
# {{{ *TeX
alias tex2txt='pandoc -f latex -t plain'
alias texmk='latexmk'         # default
alias texc='latexmk -c -norc' # leave output files (*.dvi/ps/pdf)
alias texC='latexmk -C -norc' # leave only *.tex files
# }}}
# {{{ _gnu_generic (i.e.: provides --help)
compdef _gnu_generic zathura
compdef _gnu_generic steamleaderboard
# }}}
# {{{Vim muscle memory
alias v="$VISUAL"
alias :q='exit'
alias :wq='exit'
alias :x='exit'
alias :e="$VISUAL"
alias :sp="$VISUAL -o"
alias :vsp="$VISUAL -O"
# }}}
# {{{ TODO: move to misc-scripts/make config file
# qemu auto-forward ssh
alias qemu64fwd='qemu-system-x86_64 -net nic -net user,hostfwd=tcp::2222-:22 -nographic'
alias startvm='qemu-system-x86_64 -hda ~/.local/share/qemu-drives/deb64.img -net nic -net user,hostfwd=tcp::2222-:22 -nographic'
# Steam Controller Keyboard restore
alias SCKB='cp -v ~/.backup/Steam\ keyboard/layout_english_dualtouch.txt ~/.steam/tenfoot/resource/keyboards'
function foldl {
	local f=$1 a=$2
	shift 2
	while (( $# > 0 )); do
		a="$("$f" "$a" "$1")" || return 1
		shift
	done
	echo $a
}
showargs() {
	printf '%d args\n' $#
	printf '<[ %s ]> ' "$@"
}
# }}}
#,vim:,set,foldmethod=marker:
