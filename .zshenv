export MOSH_ESCAPE_KEY="~"
(( $+_PROFILE_SOURCED )) || source "$HOME/.profile"
typeset -T GITREPOPATH gitrepopath
# prompt expansion, script filename, P: realpath, h: dirname
ZDOTDIR="${${(%):-%x}:P:h}"
# write problems in zdotdir
export NOTES_DIRECTORY="$HOME/Documents/current/notes"

if (( $+commands[nvr-visual] )); then
	# workaround opening directories
	export SUDO_EDITOR=nvim
	export GIT_EDITOR='nvr --remote-wait-silent'
	export VISUAL=nvr-visual
	export EDITOR=nvim
elif (( $+commands[nvim] )); then
	export VISUAL=nvim
	export EDITOR=nvim
elif (( $+commands[vim] )); then
	export VISUAL=vim
	export EDITOR=vim
fi
if (( $+commands[feh] )); then
	export XIVIEWER=feh
fi
